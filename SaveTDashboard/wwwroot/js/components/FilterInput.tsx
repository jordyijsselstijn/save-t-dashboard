import * as React from 'react';
import {FormEvent} from "react";

export class FilterInput extends React.Component<{onSearchResult?: Function, onChange?: Function}, {searchTerm: string}> {
    private formRef: any;
    
    constructor(props: {onSearchResult?: Function, onChange?: Function}) {
        super(props);
        this.state = {
            searchTerm: ''
        }
    }
    onValueChange = (e: any) => {
        e.preventDefault();
        this.setState({ searchTerm: e.target.value })
        this.props.onChange(e.target.value);
        
    };
    onSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.props.onSearchResult(this.state.searchTerm)
    };
    render(){
        return (
            <div className="search-bar">
                <form ref={ref => this.formRef = ref} onSubmit={this.onSubmit}>
                    <input type="text" value={this.state.searchTerm} onChange={this.onValueChange}/>
                    <img className="search-icon" src="img/search.png" alt="search"/>
                </form>
            </div>
        )
    }
}