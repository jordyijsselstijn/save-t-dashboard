import * as React from 'react';
import {iFireEngine} from "../models";

export class FireEngineInformation extends React.Component<{engine: iFireEngine}, {active: boolean}> {
    constructor(props: {engine: iFireEngine})
    {
        super(props);
        this.state = {
            active: false
        }
    }
    toggleInformation = () => {
        this.setState({ active: !this.state.active});
    };
    zoomToEngine = () => {
        window.dispatchEvent(new CustomEvent('map-zoom-on-element', { detail: { 
            latLong: [this.props.engine.location.latitude, this.props.engine.location.longitude]
            }}))
    }
    render(){
        return (<div className={`card card-elevation-1 fire-engine-info ${ this.state.active ? 'active' : ''}`} onClick={this.toggleInformation}>
            <div className="card-title">
                <img onClick={this.zoomToEngine} src="/img/fire-engine.png" alt="fire-engine"/>{this.props.engine.name}
                <span className={`availability ${ this.props.engine.availability }`}></span>
            </div>
            <div className="card-body">
                {this.props.engine.assignment}
            </div>
        </div>);
    }
}