import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Navbar, Map, FilterInput, FireEngineInformation, Marker, InfoPanel} from './';
import {iFireEngine, iIncident} from "../models";
import { FeatureFactory } from "../utils";
import {SideBarFragment} from "../Fragments/SideBarFragment";


export class App extends React.Component<{}, { incidents: iIncident[], fireEngines: iFireEngine[], _fireEngines: iFireEngine[]}> {
    private poller: any;
    private initialStartup: boolean = true;
    constructor(props: {}) {
        super(props);
        this.state = {
            incidents: [],
            fireEngines: [],
            _fireEngines: []
        }
        
    }
    onIncidentDataReceived(data: iIncident[]) {
        if (JSON.stringify(data) != JSON.stringify(this.state.incidents)) {
            if (!this.initialStartup) {
                window.dispatchEvent(new CustomEvent('notifications-received', { detail: { numberOfNewEvents: data.length - this.state.incidents.length }}));
            }
            this.initialStartup = false;

            this.setState({incidents: data});
            let geojson = {
                type: 'FeatureCollection',
                features: []
            };
            data.forEach((data: iIncident) => {
                let description = <Marker id={data.id} buttonText={"Meer info"} description={data.address.street + ' ' + data.address.postalCode} title={"Brandmelding"}></Marker>;
                let feature = FeatureFactory.createNewFeature(data.location, { title: data.type, marker: description, id: data.id });
                geojson.features.push(feature) 
            });
            window.dispatchEvent(new CustomEvent('on-new-feature-collection-received', { 
                detail: { 
                    geojson: geojson,
                    markerType: 'marker'
                }
            }));
            console.log('New data received');
        }

    }
    onFireEngineDataReceived(data: iFireEngine[]) {
        this.setState({fireEngines: data, _fireEngines: data});
        let geojson = {
            type: 'FeatureCollection',
            features: []
        };
        data.forEach((data: iFireEngine) => {
            geojson.features.push(FeatureFactory.createNewFeature(data.location, { title: data.name, marker: data.assignment,  id: data.id}))
        });
        window.dispatchEvent(new CustomEvent('on-new-feature-collection-received', {
            detail: {
                geojson: geojson,
                markerType: 'fire-engine'
            }
        }));
    }
    onDataRejected() {
        window.clearInterval(this.poller);
    }
    componentDidMount(): void {
        fetch('/api/fireengines')
            .then(res => res.json())
            .then((data: iFireEngine[]) => {
                this.onFireEngineDataReceived(data);
            });
        this.poller = setInterval(() => {
            fetch('/api/incidents')
                .then(res => res.json())
                .then((data: iIncident[]) => {
                    this.onIncidentDataReceived(data);
                })
                .catch(() => {
                    this.onDataRejected()
                })
        }, 1000);
    }
    searchChanged = (value) => {
        this.setState({
            fireEngines: this.state._fireEngines.filter(fireEngine => fireEngine.name.toLowerCase().includes(value.toLowerCase()))
        })
    };
    render(){
        let engines = this.state.fireEngines.map((engine: iFireEngine) => {
            return <li key={engine.id}><FireEngineInformation engine={engine} /></li>
        });
        return (
            <div className="app">
                <Navbar />
                <div className="main-container">
                    <SideBarFragment>
                        <FilterInput onChange={this.searchChanged}/>    
                        <ul className="list-container">
                            {engines}
                        </ul>
                    </SideBarFragment>
                    <Map
                        accessToken='pk.eyJ1Ijoiam9yZHlpanNzZWxzdGlqbjA4MzYzMTIiLCJhIjoiY2p4MjV6ejEyMGZ2MjN5bXE3dG40czVoMyJ9.51iiLdLIuuJhA4-UShnDKA'
                        styleUrl='mapbox://styles/mapbox/streets-v11'/>
                     <InfoPanel />
                </div>

            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app-root'));