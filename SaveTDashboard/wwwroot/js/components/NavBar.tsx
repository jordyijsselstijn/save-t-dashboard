import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { iIncident } from "../models";


export class Navbar extends React.Component<{}, { notificationCount: number}>
{
    
    constructor(props: {})
    {
        super(props);
        this.state = { notificationCount: 0}
    }
    componentDidMount(): void {
        window.addEventListener('notifications-received', (e: CustomEvent) => {
            this.setState({ notificationCount: e.detail.numberOfNewEvents });
        })
    }

    render() {
        return(
            <nav
                className="navbar navbar-expand-sm navbar-toggleable-sm navbar-light border-bottom box-shadow mb-3 bg-fire-red">
                <div className="container">
                    <a className="navbar-brand">Save - T</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target=".navbar-collapse" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="navbar-collapse collapse d-sm-inline-flex flex-sm-row-reverse">
                        <ul className="navbar-nav flex-grow-1">
                            {
                                this.state.notificationCount > 0 &&
                                    <div className="new-items">
                                        {this.state.notificationCount}
                                    </div>
                            }
                            <img src="/img/brandweer-logo.png" alt="logo"/>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}