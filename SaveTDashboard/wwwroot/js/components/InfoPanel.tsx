import * as React from 'react';
import {Component} from "react";
import {iIncident} from "../models";

export enum PanelState {
    OPEN = 'open',
    CLOSED = 'closed'
}

export class InfoPanel extends Component<{}, {panelState: PanelState, information: iIncident}> {
    constructor(props: {})
    {
        super(props);
        this.state = {
            panelState: PanelState.CLOSED,
            information: null
        }
    }
    componentDidMount(): void {
        window.addEventListener('marker-button-clicked', (e: CustomEvent) => {
            let id = e.detail.markerId;
            if (this.state.panelState == PanelState.CLOSED){
                this.setState({ panelState: PanelState.OPEN});
            }
            fetch('/api/incidents/' + id )
                .then(res => res.json())
                .then((res: iIncident) => {
                    this.setState({
                        information: res
                    });
                })
        })
    }

    /**
     * address:
     {…}
     city:
     "Rotterdam"
     houseNumber:
     52
     houseNumberExtension:
     null
     postalCode:
     "3069 JV"
     street:
     "Jan Jacob Slauerhoffstraat"
     date:
     "2019-06-27T01:25:36.174692+02:00"
     id:
     "17a82eba-111a-458a-a357-5d8e77a193e6"
     inhabitants:
     3
     location:
     {…}
     altitude:
     "NaN"
     course:
     "NaN"
     horizontalAccuracy:
     "NaN"

     isUnknown:
     false
     latitude:
     51.9693464
     longitude:
     4.5631669
     speed:
     "NaN"
     verticalAccuracy:
     "NaN"
     type:
     "FIRE"
     */
    closePanel = (e) => {
        this.setState({ panelState: PanelState.CLOSED });
    };
    render()
    {
        let dialog = <div></div>;
        if (this.state.information != null) {
            let { street, houseNumber, houseNumberExtension, city, postalCode } = this.state.information.address;
            dialog = <div className="information-dialog">

                <table className="table">
                    <tbody>
                    <tr>
                        <td>Straat</td>
                        <td>{street} {houseNumber}{houseNumberExtension}</td>
                    </tr>
                    <tr>
                        <td>Postcode</td>
                        <td>{postalCode}</td>
                    </tr>
                    <tr>
                        <td>Plaats</td>
                        <td>{city}</td>
                    </tr>
                    <tr>
                        <td>Personen aanwezig</td>
                        <td>{this.state.information.inhabitants}</td>
                    </tr>
                    <tr>
                        <td><a href="/stats">Brand statistieken bekijken</a></td>
                    </tr>
                    <tr>
                        <td><a href="/pictures" >Bekijk live feed</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>;
        }
        return(
            <div className={`info-panel ${this.state.panelState}`}>
                <div className="title-bar">
                    <div className="title">Informatie</div><div onClick={this.closePanel} className="close-btn">X</div>
                </div>
                {dialog}
            </div>
        )
    }
}