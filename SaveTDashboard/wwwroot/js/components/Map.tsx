import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as mapboxgl from 'mapbox-gl';
import {renderToString} from "react-dom/server";

export class Map extends React.Component<{ accessToken: string, styleUrl: string}, {markers: any[]}> {
    private mapRef: any;
    private map: mapboxgl.Map;
    
    constructor(props: {accessToken: string, styleUrl: string})
    {
        super(props);
        this.state = {
            markers: []
        };
    }
    applyFeatureCollection (markerData: any, markerType: string) {
        markerData.features.forEach((marker: any) => {
            this.addMarker(marker, markerType); 
        });
    }
    addMarker (marker: any, markerClassName: string) {
        
        const _popup = document.createElement('div');
            ReactDOM.render(<div className="marker-popup">
                {marker.properties.marker}
        </div>, _popup);
            
        const popup = new mapboxgl.Popup({ offset: 25 })
            .setDOMContent(_popup);
        
        let el = document.createElement('div');
        el.className = markerClassName;
        el.id = marker.properties.id;
        
        let markers = [...this.state.markers];
        markers.push(marker);
        this.setState({ markers: markers});
        new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)
            .setPopup(popup)
            .addTo(this.map);
    }
    componentDidMount(): void {
        (mapboxgl as any).accessToken = this.props.accessToken;
        this.map = new mapboxgl.Map({
            container: 'map',
            style: this.props.styleUrl,
            center: [4.4803861, 51.9192057],
            zoom: 10
        });

        this.map.on('load', () => {
            let popup = new mapboxgl.Popup({
                closeButton: false,
                closeOnClick: false
            });

            this.map.on('mouseenter', 'places', (e: any) => {
                this.map.getCanvas().style.cursor = 'pointer';

                let coordinates = e.features[0].geometry.coordinates.slice();
                let description = e.features[0].properties.description;

                while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                    coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                }

                popup.setLngLat(coordinates)
                    .setHTML(description)
                    .addTo(this.map);
            });

            this.map.on('mouseleave', 'places', () => {
                this.map.getCanvas().style.cursor = '';
                popup.remove();
            });
        });
        this.attachEvents();
    }
    
    attachEvents() {
        window.addEventListener('map-zoom-on-element', (e: CustomEvent) => {
            this.map.fire('click', { lngLat: e.detail.longLat})
        });
        window.addEventListener('on-new-feature-collection-received', (e: CustomEvent) => {
            this.applyFeatureCollection(e.detail.geojson, e.detail.markerType);
        });

    }
    
    render() {
        return(
            <div id="map" ref={ ref => this.mapRef = ref } style={{height: '80vh', width: '80vw'}}>
                
            </div>
        )
    }
}
