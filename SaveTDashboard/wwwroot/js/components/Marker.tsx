import * as React from 'react';
import {Component} from "react";

export class Marker extends Component<{ description: any, title: string, buttonText: string, id: string}> {
    constructor(props: { description: any, title: string, buttonText: string, id: string}) {
        super(props);
    }
    onClick = (e) => {
        window.dispatchEvent(new CustomEvent('marker-button-clicked', { detail: { markerId: this.props.id }}));   
    };
    render() {
        return (<div className="marker-inner" id={this.props.id}>
            <div className="marker-title">{this.props.title}</div>
            <div className="marker-description">
                {this.props.description}
            </div>
            <button className="marker-button" onClick={this.onClick}>{this.props.buttonText}</button>
        </div>)
    }
}