import {iGeoLocation} from "./iGeoLocation";
import {iAddress} from "./iAddress";

export interface iIncident {
    date: string,
    id: string,
    type: string,
    address: iAddress,
    location: iGeoLocation
    inhabitants: number;
}