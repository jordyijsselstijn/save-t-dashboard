export interface iAddress {
    street: string,
    houseNumber: number,
    houseNumberExtension: string,
    postalCode: string,
    city: string
}