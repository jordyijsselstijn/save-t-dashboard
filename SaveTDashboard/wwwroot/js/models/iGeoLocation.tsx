export interface iGeoLocation {
    latitude: number,
    longitude: number,
    horizontalAccuracy: number,
    verticalAccuracy: number,
    speed: number,
    course: number,
    isUnknown: boolean,
    altitude: number
}