export interface iCoordinate {
    latitude: number;
    longitude: number;
}