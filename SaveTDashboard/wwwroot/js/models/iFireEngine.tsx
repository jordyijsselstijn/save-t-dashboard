import {iGeoLocation} from "./iGeoLocation";

export enum availability {
    AVAILABLE = "available",
    EN_ROUTE = "en_route",
    UNAVAILABLE = "unavailability"
}
export interface iFireEngine {
    "id": string,
    "name": string,
    "location": iGeoLocation,
    "type":"TANKAUTOSPUIT",
    "availability": availability,
    "assignment": string
}