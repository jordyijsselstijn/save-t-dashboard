import * as React from 'react';

export class SideBarFragment extends React.Component<{}, {}> {
    constructor(props: {})
    {
        super(props);
    }
    render(){
        return (
            <div className="side-bar">
                {this.props.children}
            </div>
        )
    }
}
