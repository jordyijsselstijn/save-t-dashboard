import {iGeoLocation} from "../models";
import {Component} from "react";

export interface iFeatureProps {
    title: string,
    marker: any,
    id: string
}
export class FeatureFactory {
    static createNewFeature (geolocaton: iGeoLocation, featureProps: iFeatureProps) {
        return {
            type: 'Feature',
            geometry: {
                type: 'Point',
                coordinates: [ geolocaton.longitude, geolocaton.latitude]
            },
            properties: {
                title: featureProps.title,
                marker: featureProps.marker
            }
        }
    }
}