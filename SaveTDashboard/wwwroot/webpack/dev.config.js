const webpack = require('webpack');
const merge = require('webpack-merge');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const autoprefixer = require('autoprefixer');

const baseConfig = require('./base.config.js');

module.exports = merge(baseConfig, {
	devtool: 'eval-source-map',

	module: {
		rules: [
			{
				test: /\.(scss|css)$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader
					},
					{ loader: 'css-loader', options: { sourceMap: true, url: false } },
					{
						loader: 'postcss-loader',
						options: {

							plugins: () =>
								autoprefixer({
									browsers: ['last 3 versions', '> 1%']
								})
						}
					},
					{
						loader: 'sass-loader', options: {
							sourceMap: true,
							includePaths: ['./node_modules']
						}
					}
				]
			}
		]
	},

	plugins: [
		new MiniCssExtractPlugin({
			filename: 'app.bundle.css'
		}),
		new BrowserSyncPlugin(
			{
				open: false,
				logFileChanges: false,
				port: 3000,
				ui: { port: 3100 },
				proxy: 'http://localhost:5000',
				files: [
					{
						match: [
							path.join(__dirname, '../../assets/main.bundle.css'),
							path.join(__dirname, '../../assets/main.bundle.js'),
							path.join(__dirname, '../../**/*.cshtml'),
							path.join(__dirname, '../../**/*.html')
						],
						fn: function (event, file) {
							if (event === 'change') {
								const bs = require('browser-sync').get('bs-webpack-plugin');
								bs.reload();
							}
						}
					}
				]
			},
			{
				reload: true
			}
		)
	]
});
