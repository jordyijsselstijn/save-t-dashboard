const webpack = require('webpack');
const merge = require('webpack-merge');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const baseConfig = require('./base.config.js');

module.exports = merge(baseConfig, {
	output: {
		filename: 'app.bundle.min.js',
		path: path.join(__dirname, '../../assets')
	},

	module: {

		rules: [
			{
				test: /\.(scss|css)$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader
					},
					{ loader: 'css-loader', options: { sourceMap: true } },
					{
						loader: 'postcss-loader',
						options: {
							plugins: () =>
								autoprefixer({
									browsers: ['last 3 versions', '> 1%']
								})
						}
					},
					{
						loader: 'sass-loader', options: {
							sourceMap: true,
							includePaths: ['./node_modules']
						}
					}
				]
			}
		]

	},

	plugins: [
		new MiniCssExtractPlugin({
			filename: 'app.bundle.css'
		}),
		// Minimize JS
		new UglifyJsPlugin({ sourceMap: true, compress: true }),

		// Minify CSS
		new webpack.LoaderOptionsPlugin({
			minimize: true,
		})
	],
});