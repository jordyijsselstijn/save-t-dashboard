const webpack = require('webpack');
const path = require('path');

module.exports = {
	entry: {
		app: path.join(__dirname, '../js/app.tsx'),
		lib: path.join(__dirname, '../js/lib.tsx')
	},

	output: {
		filename: '[name].bundle.js',
		path: path.join(__dirname, '../dist'),
		library: '',
		libraryTarget: 'umd'
	},
	resolve: {
		// Add '.ts' and '.tsx' as resolvable extensions.
		extensions: [".ts", ".tsx", ".js", ".json", "scss"]
	},
	module: {
		rules: [
			{ test: /\.(tsx|ts)$/, loader: "awesome-typescript-loader" },
			{ enforce: "pre", test: /\.js$/, loader: "source-map-loader" },

			// the url-loader uses DataUrls.
			// the file-loader emits files.
			{
				test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				// Limiting the size of the woff fonts breaks font-awesome ONLY for the extract text plugin
				// loader: "url?limit=10000"
				use: "url-loader"
			},
			{
				test: /\.(ttf|eot|svg|png)(\?[\s\S]+)?$/,
				use: 'file-loader'
			},
		]
	},
	externals: {
		"react": "React",
		"react-dom": "ReactDOM",
		"jquery": "$",
		"popper.js": "Popper",
		"bootstrap": "bootstrap",
		"moment": 'moment'
	},
	plugins: [
		new webpack.EnvironmentPlugin([
			'NODE_ENV'
		])
	]
};
