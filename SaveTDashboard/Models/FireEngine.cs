using System;
using GeoCoordinatePortable;

namespace SaveTDashboard.Models
{
    public class FireEngine
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public GeoCoordinate Location { get; set; }
        public string Type { get; set; }
        public string Assignment { get; set; }
        public string Availability { get; set; }
    }
    
    public static class FireEngineType
    {
        public const string
            TANK_AUTO_SPUIT = "TANKAUTOSPUIT",
            RED_VOERTUIG = "REDVOERTUIG",
            HULPVERLENINGS_VOERTUIG = "HULPVERLENINGSVOERTUIG",
            WATER_ONGEVALLEN_VOERTUIG = "WATERONGEVALLENVOERTUIG",
            HAAKARM_VOERTUIG = "HAAKARMVOERTUIG",
            DIENST_AUTO = "DIENSTAUTO";
    }

    public static class FireEngineAvailabilityState
    {
        public const string
            AVAILABLE = "available",
            EN_ROUTE = "en_route",
            UNAVAILABLE = "unavailable";
    }
}