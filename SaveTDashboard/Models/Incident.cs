using System;
using GeoCoordinatePortable;

namespace SaveTDashboard.Models
{
    public class Incident
    {
        public DateTime Date { get; set; }
        public Guid Id { get; set; }
        public string Type { get; set; }
        public Address Address { get; set; }
        public GeoCoordinate Location { get; set; }

        public int Inhabitants { get; set; }
    }

    public class Address
    {
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        
        public char? HouseNumberExtension { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
    }

    public static class IncidentType
    {
        public const string 
            FIRE = "FIRE",
            CAR_CRASH = "CAR CRASH";
    }
}