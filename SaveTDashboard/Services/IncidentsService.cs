using System;
using System.Collections.Generic;
using GeoCoordinatePortable;
using SaveTDashboard.Models;

namespace SaveTDashboard.Services
{
    public class IncidentsService: iIncidentsService
    {
        private List<Incident> incidents { get; set; }
        public IncidentsService()
        { 
            incidents = new List<Incident>
            {
                new Incident
                {
                    Address = new Address
                    {
                        City = "Rotterdam",
                        PostalCode = "3069 JV",
                        HouseNumber = 52,
                        HouseNumberExtension = null,
                        Street = "Jan Jacob Slauerhoffstraat"
                    },
                    Location = new GeoCoordinate(51.9693464, 4.5631669),
                    Date = DateTime.Now,
                    Id = Guid.NewGuid(),
                    Type = IncidentType.FIRE,
                    Inhabitants = 3
                },
                new Incident
                {
                    Address = new Address
                    {
                        Street = "Wijhaven",
                        HouseNumber = 99,
                        HouseNumberExtension = null,
                        City = "Rotterdam",
                        PostalCode = "3011 DB"
                    },
                    Date = DateTime.Now,
                    Id = Guid.NewGuid(),
                    Type = IncidentType.FIRE,
                    Location = new GeoCoordinate(51.9173349,4.4849354),
                    Inhabitants = 250

                }
            };
        }
        public List<Incident> GetCurrentIncidents()
        {
            return incidents;
        }

        public Guid AddIncident(Incident incident)
        {
            incidents.Add(incident);
            return incident.Id;
        }

        public Incident FindById(Guid id)
        {
            return incidents.Find(incident => incident.Id == id);
        }
    }
}