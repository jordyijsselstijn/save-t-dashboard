using System;
using System.Collections.Generic;
using SaveTDashboard.Models;

namespace SaveTDashboard.Services
{
    public interface iIncidentsService
    {
        List<Incident> GetCurrentIncidents();
        Guid AddIncident(Incident incident);

        Incident FindById(Guid id);
    }
}