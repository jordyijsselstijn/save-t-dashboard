using System;
using System.Collections.Generic;
using GeoCoordinatePortable;
using SaveTDashboard.Models;

namespace SaveTDashboard.Services
{
    public class FireEngineService: iFireEngineService
    {
        private List<FireEngine> _fireEngines;
        
        public FireEngineService()
        {
            _fireEngines = new List<FireEngine>()
            {
                new FireEngine
                    
                {
                    Location = new GeoCoordinate(51.9100975,4.5444618),
                    ID = Guid.NewGuid(),
                    Name = "Fire Engine 1",
                    Type = FireEngineType.TANK_AUTO_SPUIT,
                    Assignment = "Idle",
                    Availability = FireEngineAvailabilityState.AVAILABLE
                }
                ,
                new FireEngine
                {
                    Location = new GeoCoordinate(51.9200975,4.5444618),
                    ID = Guid.NewGuid(),
                    Name = "Fire Engine 122",
                    Type = FireEngineType.TANK_AUTO_SPUIT,
                    Assignment = "En route to incident #12312112",
                    Availability = FireEngineAvailabilityState.EN_ROUTE
                },
                new FireEngine
                {
                    Location = new GeoCoordinate(51.3100975,4.5444618),
                    ID = Guid.NewGuid(),
                    Name = "Fire Engine 425",
                    Type = FireEngineType.TANK_AUTO_SPUIT,
                    Assignment = "Idle",
                    Availability = FireEngineAvailabilityState.AVAILABLE
                },
                new FireEngine
                {
                    Location = new GeoCoordinate(51.7100975,4.5444618),
                    ID = Guid.NewGuid(),
                    Name = "Fire Engine 442F",
                    Type = FireEngineType.TANK_AUTO_SPUIT,
                    Assignment = "Tasked to incident #123112221",
                    Availability = FireEngineAvailabilityState.UNAVAILABLE
                }
            };    
        }
        public List<FireEngine> GetAllFireEngines()
        {
            return _fireEngines;
        }

        public FireEngine GetFireEngineById(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}