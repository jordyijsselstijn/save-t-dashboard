using System;
using System.Collections.Generic;
using SaveTDashboard.Models;

namespace SaveTDashboard.Services
{
    public interface iFireEngineService
    {
        List<FireEngine> GetAllFireEngines();
        FireEngine GetFireEngineById(Guid id);
    }
}