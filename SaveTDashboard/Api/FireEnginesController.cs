using Microsoft.AspNetCore.Mvc;
using SaveTDashboard.Services;

namespace SaveTDashboard.Api
{
    [Route("api/[controller]")]
    public class FireEnginesController : Controller
    {
        private iFireEngineService _fireEngineService;
        
        public FireEnginesController(iFireEngineService fireEngineService)
        {
            _fireEngineService = fireEngineService;
        }
        public IActionResult Index()
        {
            return new JsonResult(_fireEngineService.GetAllFireEngines());
        }
    }
}