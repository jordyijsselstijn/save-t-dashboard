using System;
using GeoCoordinatePortable;
using Microsoft.AspNetCore.Mvc;
using SaveTDashboard.Models;
using SaveTDashboard.Services;
using SaveTDashboard.ViewModels;

namespace SaveTDashboard.Api
{
    [Route("api/[controller]")]
    public class IncidentsController : Controller
    {
        private iIncidentsService _incidentsService;
        
        public IncidentsController(iIncidentsService incidentsService)
        {
            _incidentsService = incidentsService;
        }
        public IActionResult Index()
        {
            return new JsonResult(_incidentsService.GetCurrentIncidents());
        }

        [HttpGet("{id}")]
        public IActionResult GetById(Guid id)
        {
            return new JsonResult(_incidentsService.FindById(id));
        }

        [HttpPost]
        public IActionResult AddIncident([FromBody]SaveTNotification notification)
        {
            Incident incident = new Incident
            {
                Address = notification.Address,
                Date = DateTime.Now,
                Id = Guid.NewGuid(),
                Type = notification.Type,
                Location = new GeoCoordinate(notification.Latitude,notification.Longitude),
                Inhabitants = notification.Inhabitants
            };
            
            return new JsonResult(new { result = "SUCCESS", id = _incidentsService.AddIncident(incident)});
        }
    }
}