using SaveTDashboard.Models;

namespace SaveTDashboard.ViewModels
{
    public class SaveTNotification
    {
        public Address Address { get; set; }
        public string Type { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public int Inhabitants { get; set; }
    }
}

